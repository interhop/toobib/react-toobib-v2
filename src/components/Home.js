import React from "react";
import AnnuaireForm from "./AnnuaireForm";
import Loader from "./business-components/Loader";

const Home = ({ data, isLoading, ...props }) => {
  const currentPageResults = data !== null && data.data.providers;
  return (
    <>
      {isLoading && <Loader />}
      {!isLoading && (
        <>
          <AnnuaireForm {...props} />
          {data !== null && currentPageResults.length > 0 && (
            <ul>
              {currentPageResults.map((result) => {
                return (
                  <li key={result.id}>
                    {result.prenom} {result.nom}
                  </li>
                );
              })}
            </ul>
          )}
        </>
      )}
    </>
  );
};

export default Home;
