import React from "react";

const Header = () => {
  return (
    <header className="main-template-header flex-col-c nfc-mt2 pad3">
      <h1>Annuaire</h1>
      <span>Trouvez votre praticien.ne</span>
    </header>
  )
}

export default Header;