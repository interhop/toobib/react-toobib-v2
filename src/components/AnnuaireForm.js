import React from "react";
import { Formik, Form } from "formik";
import TextInput from "./../components/business-components/TextInput";
import Button from "./../components/business-components/Button";
import { searchSchema } from "./../utils/yup";

const AnnuaireForm = ({ handleAnnuaireFormSubmit }) => {
  return (
    <>
      <Formik
        initialValues={{ search: "" }}
        onSubmit={(values) => handleAnnuaireFormSubmit(values)}
        validationSchema={searchSchema}
      >
        {() => (
          <Form
            className="flex-asc flex flex-col nfc-mt3"
            noValidate={true}
            spellCheck={false}
          >
            <TextInput name="search" label="Recherche" />
            <Button className="button flex-asc" type="submit">
              Envoyer
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default AnnuaireForm;
