import React from "react";

const Footer = () => {
  return (
    <footer className="main-template-footer flex flex-jcfe flex-aic pad3">
      <small>Un projet développé par l'association <a href="https://interhop.org/">Interhop</a></small>
    </footer>
  )
}

export default Footer;