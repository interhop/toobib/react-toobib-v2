import React from "react";

const InputError = ({message}) => {
  return (
    <span className="inputerror italic">{message}</span>
  )
}

InputError.defaultProps = {
  message: "Input error message"
}

export default InputError;