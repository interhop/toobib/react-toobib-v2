import React from "react";
import Spinner from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const Loader = ({ color, height, type, width }) => {
  return (
    <div className="loader flex-row-c flex-1">
      <Spinner color={color} height={height} type={type} width={width} />
    </div>
  );
};

Loader.defaultProps = {
  color: "hsl(0, 0%, 20%)",
  height: 70,
  type: "Oval",
  width: 70,
};

export default Loader;
