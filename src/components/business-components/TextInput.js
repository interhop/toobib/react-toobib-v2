import React from "react";
import classNames from "classnames";
import InputError from "./InputError";
import { useField } from "formik";

const FormTextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  const labelClassName = classNames("label", {
    "visually-hidden": props.hideLabel,
  });
  const inputClassName = classNames("input", {
    "error": meta.error && meta.touched,
  });
  return (
    <div className="textinput flex flex-col">
      <label className={labelClassName} htmlFor={field.name}>
        {label}
      </label>
      <input
        className={inputClassName}
        name={field.name}
        type="text"
        value={field.value}
        {...field}
        {...props}
      />
      {meta.error && meta.touched && <InputError message={meta.error} />}
    </div>
  );
};

export default FormTextInput;