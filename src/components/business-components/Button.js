import React from "react";
import classNames from "classnames";

const Button = ({ children, disabled, size, type, variant, ...props }) => {
  const buttonClassName = classNames("button", props.className, {
    primary: variant === "primary",
    secondary: variant === "secondary",
    small: size === "small",
    medium: size === "medium",
    large: size === "large",
  });
  return (
    <button className={buttonClassName} disabled={disabled} type={type}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  children: "Button",
  disabled: false,
  size: "medium",
  type: "button",
  variant: "primary"
}

export default Button;
