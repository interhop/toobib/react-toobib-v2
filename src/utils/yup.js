const yup = require("yup");

const searchSchema = yup.object().shape({
  search: yup
    .string()
    .required("Une valeur est attendue")
});

export {
  searchSchema,
};
