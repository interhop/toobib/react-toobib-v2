import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import axios from "axios";
import Home from "./components/Home";
import MainTemplate from "./templates/MainTemplate";

const App = () => {
  const [isLoading, setLoadingStatus] = useState(false);
  const [data, storeData] = useState(null);
  const [resultsPage, setResultsPage] = useState(1);

  const handleAnnuaireFormSubmit = (values) => {
    setLoadingStatus(true);

    const fetchUrl = `${
      process.env.REACT_APP_ANNUAIRE_APIBASEURL
    }${values.search.trim()}&page=1`;

    axios({
      method: "get",
      url: fetchUrl,
    })
      .then((response) => {
        console.log(response);
        storeData(response);
      })
      .catch((error) => {
        console.error(error);
      })
      .then(() => {
        setLoadingStatus(false);
      });
  };

  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          render={() => (
            <MainTemplate>
              <Home
                data={data}
                handleAnnuaireFormSubmit={handleAnnuaireFormSubmit}
                isLoading={isLoading}
              />
            </MainTemplate>
          )}
        />
        <Route
          path="/toobibPage/:Id"
          render={() => (
            <MainTemplate>
              <div>Toobib page</div>
            </MainTemplate>
          )}
        />
      </Switch>
    </Router>
  );
};

export default App;
