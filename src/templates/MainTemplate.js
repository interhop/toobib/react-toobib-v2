import React from "react";
import Header from "./../components/Header";
import Footer from "./../components/Footer";

const MainTemplate = ({ children }) => {
  return (
    <>
      <Header />
      <main className="main-template-main flex-1 flex flex-col">
        <div className="main-template-main-inner flex-asc flex-1 flex flex-col nfc-mt3 pad3">
          {children}
        </div>
      </main>
      <Footer />
    </>
  );
};

export default MainTemplate;
